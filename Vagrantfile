#YAML
require "yaml"

#If your Vagrant version is lower than 1.5, you can still use this provisioning
#by commenting or removing the line below and providing the config.vm.box_url parameter,
#if it's not already defined in this Vagrantfile. Keep in mind that you won't be able
#to use the Vagrant Cloud and other newer Vagrant features.
Vagrant.require_version ">= 1.5"

# Check to determine whether we're on a windows or linux/os-x host,
# later on we use this to launch ansible in the supported way
# source: https://stackoverflow.com/questions/2108727/which-in-ruby-checking-if-program-exists-in-path-from-ruby
def which(cmd)
    exts = ENV['PATHEXT'] ? ENV['PATHEXT'].split(';') : ['']
    ENV['PATH'].split(File::PATH_SEPARATOR).each do |path|
        exts.each { |ext|
            exe = File.join(path, "#{cmd}#{ext}")
            return exe if File.executable? exe
        }
    end
    return nil
end

Vagrant.configure("2") do |config|

    _conf = YAML.load(
        File.open(
        File.join(File.dirname(__FILE__), '/ansible/vars/all.yml'),
        File::RDONLY
        ).read
    )

    config.vm.provider :virtualbox do |v|
        v.name = _conf["vagrant_local"]["vm"]["hostname"]
        v.customize [
            "modifyvm", :id,
            "--name", _conf["vagrant_local"]["vm"]["hostname"],
            "--memory", _conf["vagrant_local"]["vm"]["memory"],
            "--natdnshostresolver1", "on",
            "--cpus", 1,
        ]
    end

    config.vm.box = _conf["vagrant_local"]["vm"]["base_box"]

    config.vm.network "forwarded_port", guest: 80, host: 8080
    config.vm.network :private_network, ip: _conf["vagrant_local"]["vm"]["ip"]
    config.ssh.forward_agent = true

    # If ansible is in your path it will provision from your HOST machine
    # If ansible is not found in the path it will be instaled in the VM and provisioned from there
    if which('ansible-playbook')
        config.vm.provision "ansible" do |ansible|
            ansible.playbook = "ansible/playbook.yml"
            ansible.inventory_path = "ansible/inventories/dev"
            ansible.limit = 'all'
        end
    else
        config.vm.provision :shell, path: "ansible/windows.sh", args: [_conf["vagrant_local"]["vm"]["hostname"]]
    end

    config.vm.synced_folder _conf["vagrant_local"]["vm"]["sharedfolder"],
            _conf["apache"]["docroot"], :create => "true", owner:"www-data", group:"www-data",
            mount_options:["dmode=775", "fmode=775"]

    if Vagrant.has_plugin?('vagrant-hostsupdater')
        config.hostsupdater.remove_on_suspend = true
    end

    if Vagrant.has_plugin?('vagrant-vbguest')
        config.vbguest.auto_update = false
    end

    config.vm.provider :virtualbox do |vb|
        vb.customize ["modifyvm", :id, "--natdnshostresolver1", "on"]
    end

    config.push.define "ftp" do |push|
      push.host = _conf["vagrant_local"]["vm"]["ftp"]
      push.username = "root"
      push.password = "root"
    end

end
