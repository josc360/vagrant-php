# Default Apache virtualhost template

<VirtualHost *:80>
    ServerAdmin webmaster@localhost
    DocumentRoot {{ apache.docroot }}
    ServerName {{ apache.servername }}

    <Directory {{ apache.docroot }}>
        Options -Indexes +Includes +FollowSymLinks +Multiviews
        AllowOverride All
        Order allow,deny
        allow from all
    </Directory>
</VirtualHost>
